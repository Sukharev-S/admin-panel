import "../../helpers/iframeLoader.js"; // фикс для старых браузеров, загрузки iframe
import React, {Component} from 'react';
import axios from 'axios';

export default class Editor extends Component {
  constructor() {
    super();
    this.currentPage = "index.html";
    this.state = {
      pageList: [],
      newPageName: ""
    }
    this.createNewPage = this.createNewPage.bind(this);
  }

  componentDidMount() {
    this.init(this.currentPage);
  }

  init(page) {
    this.iframe = document.querySelector('iframe');
    this.open(page);
    this.loadPageList();
  }

  open(page) {
    this.currentPage = page;
    axios
      .get(`../${page}?rnd=${Math.random()}`)
      .then(res => this.parseStrToDom(res.data))
      .then(this.wrapTextNodes)
      .then(dom => {
        this.virtualDom = dom;     // Тут будет лежать чистая копия страницы
        return dom;
      })
      .then(this.serializeDOMToString)
      .then(html => axios.post("./api/saveTempPage.php", {html}))
      .then(() => this.iframe.load("../temp.html"))
      .then(() => this.enableEditing())
  }

  save() {
    const newDom = this.virtualDom.cloneNode(this.virtualDom);
    this.unwrapTextNodes(newDom);
    const html = this.serializeDOMToString(newDom);
    axios
      .post("./api/savePage.php", {pageName: this.currentPage, html})
  }

  enableEditing() {
    this.iframe.contentDocument.body.querySelectorAll("text-editor").forEach(element => {
      element.contentEditable = "true";
      element.addEventListener("input", () => {
        this.onTextEdit(element);
      })
    })
  }

  onTextEdit(element) {
    const id = element.getAttribute("nodeId");
    this.virtualDom.body.querySelector(`[nodeId="${id}"]`).innerHTML = element.innerHTML;
  }

  parseStrToDom(str) {          // Парсит строку в DOM структуру
    const parser = new DOMParser();
    return parser.parseFromString(str, "text/html");
  }

  wrapTextNodes(dom) {
    const body = dom.body;
    let textNodes = [];
    function recursy(element) {
      element.childNodes.forEach(node => {
        if (node.nodeName === "#text" && node.nodeValue.replace(/\s+/g, "").length > 0) {
          textNodes.push(node);
        } else {
          recursy(node);
        }
      })
    }
    recursy(body);
    textNodes.forEach((node, i) => {
      const wrapper = dom.createElement('text-editor');
      node.parentNode.replaceChild(wrapper, node);
      wrapper.appendChild(node);
      wrapper.setAttribute("nodeId", i);
    });
    return dom;
  }

  serializeDOMToString(dom) {           // Парсит DOM структуру в строку для передачи по POST запросу
    const serializer = new XMLSerializer();
    return serializer.serializeToString(dom);
  }

  unwrapTextNodes(dom) {
    dom.body.querySelectorAll("text-editor").forEach(element => {
      element.parentNode.replaceChild(element.firstChild, element);
    });
  }

  loadPageList() {
    axios
      .get("./api")
      .then(res => this.setState({pageList: res.data}))
  }

  createNewPage() {
    console.log("До: " + this.state.newPageName)
    axios
      .post("./api/createNewPage.php", {"name": this.state.newPageName})
      .then(this.loadPageList())
      .catch(() => alert("Страница уже существует!"));
    console.log("После: " + this.state.newPageName)
  }

  deletePage(page) {
    axios
      .post("./api/deletePage.php", {
        "name": page
      })
      .then(this.loadPageList())
      .catch(() => alert("Страницы не существует!"));
  }

  render() {
    // window.state = this.state;
    // const {pageList} = this.state;
    // const pages = pageList.map((page, i) => {
    //   return(
    //     <h1 key={i}>{page}
    //     <a 
    //     href="#"
    //     onClick={() => this.deletePage(page)}>(x)</a>
    //     </h1>
    //   )
    // });
    return ( 
      <>
        <button onClick={() => this.save()}>Save</button>
        <iframe src = {this.currentPage} frameBorder = "0"> </iframe>      
      </>

      
      // <>
      //   <input 
      //     onChange={(e) => {this.setState({newPageName: e.target.value})}} 
      //     type="text" />
      //   <button onClick={this.createNewPage}>Создать страницу</button>
      //   {pages}
      // </>
    )
  }
}